<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_product',
        'id_category',
        'price_product',
        'description_product',
        'rating_product',
        'stock_product'
    ];

    protected $hidden = [
        'id_category'
    ];

    //menkonfigurasi tipe data
    protected $casts = [
        'rating_product'=> 'double',
        'stock_product' => 'integer'
    ];
}

