<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'quantity_order',
        'total_order',
        'id_product',
        'id_category',
    ];

    protected $hidden = [
        'id_product',
        'id_category'
    ];

    //menkonfigurasi tipe data
    protected $casts = [
        'quantity_order'=> 'integer',
        'total_order' => 'integer'
    ];

}
