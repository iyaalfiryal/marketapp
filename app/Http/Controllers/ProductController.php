<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createProduct(Request $request)
    {
        $data = $request->all();

        try{
            $product = new Product();
            $product->name_product = $data['name_product'];
            $product->price_product = $data['price_product'];
            $product->description_product = $data['description_product'];
            $product->rating_product = $data['rating_product'];
            $product->stock_product = $data['stock_product'];
            $product->id_category = $data['id_category'];

            //save data ke database
            $product->save();
            $status = 'success';
            return response()->json(compact('status', 'product'), 200);

        } catch (\Throwable $th) {
            $status = 'unsuccess';
            return response()->json(compact('status', 'th'), 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showProduct($id)
    {
        return Product::findOrFail($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProduct(Request $request, $id)
    {
        $data = $request->all();

        try{
            $product = Product::findOrFail($id);
            $product->name_product = $data['name_product'];
            $product->price_product = $data['price_product'];
            $product->description_product = $data['description_product'];
            $product->rating_product = $data['rating_product'];
            $product->stock_product = $data['stock_product'];
            $product->id_category = $data['id_category'];

            //save data ke database
            $product->save();
            $status = 'success';
            return response()->json(compact('status', 'product'), 200);

        } catch (\Throwable $th) {
            $status = 'unsuccess';
            return response()->json(compact('status', 'th'), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteProduct($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        $status = 'deleted';
        return response()->json(compact('status'), 200);
    }
}
